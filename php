ARG DOCKER_TAG
FROM php:${DOCKER_TAG}

RUN apk add --update --no-cache curl git jq bash xz ca-certificates openssh-client patch

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer --2

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions zip
